FROM maven AS MAVEN_BUILD
COPY application /var/www/application
COPY common /var/www/common
COPY domain /var/www/domain
COPY persistence /var/www/persistence
COPY pom.xml /var/www/
WORKDIR /var/www/
EXPOSE 8080
RUN ls
RUN mvn install package


FROM openjdk:16-jdk-alpine3.12
COPY --from=MAVEN_BUILD /var/www/application/target/application-0.0.1-SNAPSHOT-exec.jar /application-0.0.1-SNAPSHOT-exec.jar
CMD ["java", "-jar", "-Dspring.profiles.active=sqlserver", "/application-0.0.1-SNAPSHOT-exec.jar"]
package com.siscont.domain.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class ContaContabil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;

    public ContaContabil() {
    }

    public ContaContabil(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public ContaContabil(String descricao) {
        this.descricao = descricao;
    }

    public ContaContabil(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (ContaContabil) o;
        return Objects.equals(id, that.id) && Objects.equals(descricao, that.descricao);
    }
}

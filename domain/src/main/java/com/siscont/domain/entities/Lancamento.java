package com.siscont.domain.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Lancamento implements Comparable<Lancamento> {
    @Id
    private UUID id;
    @Column(nullable = false)
    private LocalDate data;
    @Column(nullable = false, scale = 2)
    private BigDecimal valor;
    @JoinColumn(nullable = false, updatable = false)
    @ManyToOne
    private ContaContabil contaContabil;
    @Column(unique = true, updatable = false)
    private int hashCode;

    public Lancamento() {
    }

    public Lancamento(UUID id, LocalDate data, BigDecimal valor) {
        this.id = id;
        this.data = data;
        this.valor = valor;
    }

    public Lancamento(LocalDate data, BigDecimal valor, ContaContabil contaContabil) {
        this.id = UUID.randomUUID();
        this.data = data;
        this.valor = valor;
        this.contaContabil = contaContabil;
        this.hashCode = hashCode();
    }

    public UUID getId() {
        return id;
    }

    public LocalDate getData() {
        return data;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public ContaContabil getContaContabil() {
        return contaContabil;
    }

    public void setContaContabil(ContaContabil contaContabil) {
        this.contaContabil = contaContabil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lancamento that = (Lancamento) o;
        return Objects.equals(id, that.id) && Objects.equals(data, that.data) && Objects.equals(valor
                .setScale(2, RoundingMode.HALF_EVEN), that.valor
                .setScale(2, RoundingMode.HALF_EVEN)) &&
                Objects.equals(contaContabil, that.contaContabil);
    }

    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            this.hashCode = Objects.hash(data, valor, contaContabil.getId(), LocalDateTime.now().withNano(0));
        }

        return this.hashCode;
    }

    @Override
    public int compareTo(Lancamento lancamento) {
        if (this.hashCode == lancamento.hashCode()) {
            return 0;
        }

        return -1;
    }
}

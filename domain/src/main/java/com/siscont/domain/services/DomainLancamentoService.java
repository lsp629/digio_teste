package com.siscont.domain.services;

import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.entities.Lancamento;
import com.siscont.domain.interfaces.ContaContabilRepository;
import com.siscont.domain.interfaces.LancamentoRepository;
import com.siscont.domain.interfaces.LancamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DomainLancamentoService implements LancamentoService {
    private final LancamentoRepository lancamentoRepository;
    private final ContaContabilRepository contaContabilRepository;
    private final Logger logger = LoggerFactory.getLogger(DomainLancamentoService.class);

    public DomainLancamentoService(LancamentoRepository lancamentoRepository,
                                   ContaContabilRepository contaContabilRepository) {
        this.lancamentoRepository = lancamentoRepository;
        this.contaContabilRepository = contaContabilRepository;
    }

    @Override
    public String create(Lancamento lancamento, Long contaContabilId) {
        logger.info("Criando lancamento", lancamento, contaContabilId);
        var contaContabil = isContaExists(contaContabilId);
        lancamento.setContaContabil(contaContabil);
        return create(lancamento);
    }

    private ContaContabil isContaExists(Long contaContabilId) {
        var contaContabil = contaContabilRepository.fetchOne(contaContabilId)
                .orElseThrow(() -> new EntityNotFoundException("Conta contábil não existe"));
        return contaContabil;
    }

    @Override
    public String create(Lancamento lancamento) {
        return lancamentoRepository.createLancamento(lancamento).getId().toString();
    }

    @Override
    public List<Lancamento> fetchAll() {
        return lancamentoRepository.fetchAllLancamentos();
    }

    @Override
    public Optional<Lancamento> fetchOne(String id) {
        return lancamentoRepository.fetchOneLancamento(UUID.fromString(id));
    }

    @Override
    public List<Lancamento> fetchByContaContabil(Long id) {
        isContaExists(id);
        return lancamentoRepository.fetchLancamentoByContaContabil(id);
    }

    @Override
    public List<Lancamento> fetch(Long contaContabilId) {
        if (contaContabilId == null) {
            return fetchAll();
        }

        return fetchByContaContabil(contaContabilId);
    }
}

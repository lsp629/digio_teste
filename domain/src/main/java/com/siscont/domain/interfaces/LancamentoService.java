package com.siscont.domain.interfaces;

import com.siscont.domain.entities.Lancamento;

import java.util.List;
import java.util.Optional;

public interface LancamentoService {
    String create(Lancamento lancamento, Long contaContabilId);
    String create(Lancamento lancamento);
    List<Lancamento> fetchAll();
    Optional<Lancamento> fetchOne(String id);
    List<Lancamento> fetchByContaContabil(Long id);
    List<Lancamento> fetch(Long contaContabilId);
}

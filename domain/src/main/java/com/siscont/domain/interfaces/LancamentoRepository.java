package com.siscont.domain.interfaces;

import com.siscont.domain.entities.Lancamento;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LancamentoRepository {
    Lancamento createLancamento(Lancamento lancamento);
    List<Lancamento> fetchAllLancamentos();
    Optional<Lancamento> fetchOneLancamento(String id);
    Optional<Lancamento> fetchOneLancamento(UUID id);
    List<Lancamento> fetchLancamentoByContaContabil(Long id);
    void purge();
}

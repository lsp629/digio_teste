package com.siscont.domain.interfaces;

import com.siscont.domain.entities.ContaContabil;

import java.util.Optional;

public interface ContaContabilRepository {

    ContaContabil create(ContaContabil contaContabil);
    void purge();
    Optional<ContaContabil> fetchOne(Long id);
}

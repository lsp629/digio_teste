# Projeto Teste Digio

Este projeto tem por objetivo representar a minha avalição na Digio.

# API

A documentação da API está disponível [Aqui](https://documenter.getpostman.com/view/3402121/TVzSicMn)


### Descrição
Este projeto foi desenvolvido seguindo o modelo de arquitetura Hexagonal e os princípios do SOLID.
Fiz testes behaviorais me utilizando de BDD.

#### Tecnologias
Java, Maven, Spring Boot, Spring Data Jpa, Cucumber, Sql Server e Docker

#### Por que modularizar?
Caso modularizemos de forma correta, a modularização pode ser uma arma poderosa no desenvolvimento de sotware:

Peguemos a camada de persistência como exemplo:
Neste projeto, me utilizei do Sql Server para a persitência. 
A Camada de persistência implementa um contrato. Uma interface definida na camada de domínio.
Esta interface, explica o que ela espera da camada de persistência.
Desta forma, caso se mude o banco de dados, ou quaquer regra de persistência elas não afetarão o nosso
negócio.

- Podemos ainda trocar o módulo por outro desde que ele se atenha ao contrato.
- Podemos tornar o código facilmente reutilizável
- Podemos dividir facilmente o trabalho que uma pessoa faria para que várias pessoas trabalhem desde que tenhamos o contrato muito bem definido.
- Podemos melhorar a qualidade dos testes.
- Diminuimos o acomplamento e aumentamos a coesão.
  
### Por que utilizar BDD?

Sinceramente, é bem chato usar... escrever o comportamento da aplicação e os testes leva tempo.
Porém, temos 3 ganhos que eu consigo lembrar agora (ta tarde, fiz horas extras, 3 ta bom kkk), muito importantes: 

- Documentação. Temos um local muito fácil e prático para consultarmos o negócio.
- Comunicação. O BDD pode ser utiizado por BAs, POs, e etc. E no processo da documentação podemos definir
a linguagem universal que poderá ser utilizada no projeto: a linguagem ubíqua.
- Refinamento do desenvolvimento. Como o TDD, temos uma maior garantia de que estamos desenvolvendo corretamente.
E a despeito do TDD temos a validação escrita dos comportamentos que esperamos o que dificulta menos na hora de escrever
testes.


## Quando devemos aplicar os conceitos apresentados neste projeto?

Depende do projeto. Existem projetos que são simples demais para muita abstração e podem ser resolvidos
rapidamente com menos recursos e tecnologias. O dia-a-dia do projeto também impacta muito a qualidade do desenvolvimento infelizmente.

Mas algumas práticas são essenciais. O SOLID é uma delas.

## Por que usar a final keyword

- A ideia do código é ser self-documented. Isso significa que utilizar uma final keyword expressa o que ela realmente significa para o objeto. Para o controller
espero expressar que uma classe de serviço é essencial para o seu funcionamento.
  
Uma referência interessante é: [Uso liberal da final keyword](http://www.javapractices.com/topic/TopicAction.do?Id=23)

- A final impede que alguém altere um objeto que não deve ser alterado. Isso é óbvio. Mas mesmo assim, erros e desatenções acontecem.

## Qual a importância do Adtaper para o Hexagonal architecture?
A arquitetura hexagonal utiliza-se de adapters para poder manter a flexiblidade.
Neste caso o uso dos adpaters permite reduzir significativamente o grau de acoplamento.
Pode-se por exemplo, utiliar a interface criada neste projeto 'LancamentoRepository' para adaptar varios tipos de bancos de dados diferentes.
Poderia-se utilizar um mongoDb, ou qualquer outro recurso persistente e o custo para a camada de domínio seria zero.

## Por que não utilizar field injection

++ less code to write
-- unsafe code
- more complicated to test

Uma referência interessante é: [Why field injection is evil](http://olivergierke.de/2013/11/why-field-injection-is-evil/)

Essa é uma ótima referência já que Oliver Drotbohm um *Java Champion* é uma ótima referência.

#### Referências:

[Domain-Driven Design: Atacando as Complexidades no Coração do Software](https://www.amazon.com.br/Domain-Driven-Design-Eric-Evans/dp/8550800651/ref=sr_1_1?__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=HTITTZ19BLQL&dchild=1&keywords=eric+evans&qid=1610410248&sprefix=eric+evan%2Caps%2C290&sr=8-1)

[DDD – Introdução a Domain Driven Design](http://www.agileandart.com/2010/07/16/ddd-introducao-a-domain-driven-design/)

[https://www.baeldung.com/hexagonal-architecture-ddd-spring](https://www.baeldung.com/hexagonal-architecture-ddd-spring)

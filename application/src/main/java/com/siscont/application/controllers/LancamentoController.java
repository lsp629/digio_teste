package com.siscont.application.controllers;

import com.siscont.application.controllers.exceptions.NotFoundException;
import com.siscont.application.controllers.models.CreateLancamentoRequest;
import com.siscont.application.controllers.models.CreateLancamentoResponse;
import com.siscont.application.controllers.models.LancamentoResponse;
import com.siscont.application.controllers.models.StatsResponse;
import com.siscont.application.operations.StatsCalculator;
import com.siscont.domain.entities.Lancamento;
import com.siscont.domain.interfaces.LancamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/lancamentos-contabeis")
@Validated
public class LancamentoController {
    private final LancamentoService lancamentoService;
    //static com um private faria diferença se eu tivesse que compartilhar o valor com múltiplas instâncias da mesma classe.
    //O argumento de economia de memória não se aplica aqui já que o objeto logger vai ocupar a mesma quantidade de memória.
    //A diferença real é a de que as variaveis marcadas como static neste caso estarão apenas sendo instanciadas primeiro.
    private final Logger logger = LoggerFactory.getLogger(LancamentoController.class);

    public LancamentoController(LancamentoService lancamentoService) {
        this.lancamentoService = lancamentoService;
    }

    @PostMapping
    public ResponseEntity<CreateLancamentoResponse> createLancamento(@Valid @RequestBody CreateLancamentoRequest request) {
        logger.info("Criando lancamento", request);
        return new ResponseEntity<>(new CreateLancamentoResponse(lancamentoService.create(request.get(),
                request.getContaContabil())), HttpStatus.CREATED);
    }

    @GetMapping("/{lancamentoId}")
    public ResponseEntity<LancamentoResponse> fetchLancamentoById(@PathVariable String lancamentoId) {
        logger.info("Buscando lancamento pelo id", lancamentoId);
        return new ResponseEntity<>(new LancamentoResponse(lancamentoService.fetchOne(lancamentoId)
                .orElseThrow(NotFoundException::lancamentoNaoEncontrado)),
                HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<LancamentoResponse>> fetchLancametoByContaContabilId(
            @RequestParam Long contaContabil) {
        logger.info("Buscando lancamento - contaContabil", contaContabil);
        return new ResponseEntity<>(mapLancamentosToResponse(lancamentoService.fetch(contaContabil)),
                HttpStatus.OK);
    }

    @GetMapping("/stats")
    public ResponseEntity<StatsResponse> calculateStats(@RequestParam(required = false) Long contaContabil) {
        logger.info("Criado estatísticas", contaContabil);
        return new ResponseEntity<>(new StatsCalculator(lancamentoService.fetch(contaContabil)).calculateStats(),
                HttpStatus.OK);
    }

    private List<LancamentoResponse> mapLancamentosToResponse(List<Lancamento> lancamentos) {
        return lancamentos.parallelStream().map(LancamentoResponse::new).collect(Collectors.toList());
    }
}

package com.siscont.application.controllers.models;

import java.math.BigDecimal;
import java.util.Objects;

public class StatsResponse {
    private BigDecimal soma;
    private BigDecimal min;
    private BigDecimal max;
    private BigDecimal media;
    private Integer qtde;

    public StatsResponse() {
    }

    public StatsResponse(BigDecimal soma, BigDecimal min, BigDecimal max, BigDecimal media, Integer qtde) {
        this.soma = soma;
        this.min = min;
        this.max = max;
        this.media = media;
        this.qtde = qtde;
    }

    public BigDecimal getSoma() {
        return soma;
    }

    public void setSoma(BigDecimal soma) {
        this.soma = soma;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public BigDecimal getMedia() {
        return media;
    }

    public void setMedia(BigDecimal media) {
        this.media = media;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (StatsResponse) o;
        return Objects.equals(soma, that.soma) && Objects.equals(min, that.min) && Objects.equals(max, that.max)
                && Objects.equals(media, that.media) && Objects.equals(qtde, that.qtde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(soma, min, max, media, qtde);
    }
}

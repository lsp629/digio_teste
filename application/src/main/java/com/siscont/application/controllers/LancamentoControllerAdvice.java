package com.siscont.application.controllers;

import com.siscont.application.controllers.exceptions.NotFoundException;
import com.siscont.application.controllers.models.ErrorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

@ControllerAdvice
public class LancamentoControllerAdvice {
    //Colocar static num singleton não parece ter alguma diferença significativa
    private final Logger logger = LoggerFactory.getLogger(LancamentoControllerAdvice.class);
    private static final String INVALID_REQUEST_BODY_MESSAGE = "O body da requisição é inválido";
    private static final String RESTRICAO_DADOS_MESSAGE = "O Lancamento não foi salvo. Verifique se o mesmo foi " +
            "duplicado ou verifique os requisitos da api.";
    private static  final String CONTA_CONTABIL_NAME = "contaContabil";
    private static final String MISSING_CONTA_CONTABIL = format("O parâmetro requerido '%s' não foi enviado",
            CONTA_CONTABIL_NAME);

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleInvalidBody(HttpMessageNotReadableException ex) {
        logger.error(INVALID_REQUEST_BODY_MESSAGE, ex);
        return new ResponseEntity<>(new ErrorsResponse(INVALID_REQUEST_BODY_MESSAGE, null),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleNotFoundDomain(EntityNotFoundException ex) {
        logger.error(ex.getMessage(), ex);
        return handleNotFoundElements(new NotFoundException(ex.getMessage()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleMissingParameter(MissingServletRequestParameterException ex) {
        logger.error(ex.getMessage(), ex);
        String message = ex.getMessage().contains(CONTA_CONTABIL_NAME) ? MISSING_CONTA_CONTABIL : ex.getMessage();
        return new ResponseEntity<>(new ErrorsResponse(message, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleNotDuplicatedData(DataIntegrityViolationException ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorsResponse(RESTRICAO_DADOS_MESSAGE, null), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleInvalidBodyFields(MethodArgumentNotValidException ex) {
        logger.error(ex.getMessage(), ex);
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return new ResponseEntity<>(new ErrorsResponse(INVALID_REQUEST_BODY_MESSAGE, errors),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorsResponse> handleNotFoundElements(NotFoundException ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorsResponse(ex.getMessage(), null), HttpStatus.NOT_FOUND);
    }
}

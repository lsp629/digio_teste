package com.siscont.application.controllers.models;

public class CreateLancamentoResponse {
    private String id;

    public CreateLancamentoResponse(String id) {
        this.id = id;
    }

    public CreateLancamentoResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.siscont.application.controllers.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.siscont.application.controllers.models.serializers.LocalDateDeserializer;
import com.siscont.application.controllers.models.serializers.LocalDateSerializer;
import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.entities.Lancamento;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateLancamentoRequest {
    @NotNull(message = "O campo data não deve ser nulo")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate data;
    @NotNull(message = "O campo contaContabil não deve ser nulo")
    private Long contaContabil;
    @NotNull(message = "O campo valor não deve ser nulo")
    private BigDecimal valor;

    public CreateLancamentoRequest(LocalDate data, Long contaContabil, BigDecimal valor) {
        this.data = data;
        this.contaContabil = contaContabil;
        this.valor = valor;
    }

    public CreateLancamentoRequest() {
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public void setContaContabil(Long contaContabil) {
        this.contaContabil = contaContabil;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public LocalDate getData() {
        return data;
    }

    public Long getContaContabil() {
        return contaContabil;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public Lancamento get() {
        return new Lancamento(data, valor, new ContaContabil(contaContabil));
    }

    @Override
    public String toString() {
        return "CreateLancamento{" +
                "data=" + data +
                ", contaContabil=" + contaContabil +
                ", valor=" + valor +
                '}';
    }
}

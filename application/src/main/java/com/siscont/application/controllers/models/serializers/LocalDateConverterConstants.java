package com.siscont.application.controllers.models.serializers;

import java.time.format.DateTimeFormatter;

public class LocalDateConverterConstants {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
}

package com.siscont.application.controllers.exceptions;

public class NotFoundException extends RuntimeException {
    private static final String LANCAMENTO_NAO_ENCONTRADO = "Lançamento não encontrado";
    private static final String CONTA_INEXISTENTE = "Conta inexistente";

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public static NotFoundException lancamentoNaoEncontrado() {
        return new NotFoundException(LANCAMENTO_NAO_ENCONTRADO);
    }

    public static NotFoundException contaInexistente() {
        return new NotFoundException(CONTA_INEXISTENTE);
    }
}

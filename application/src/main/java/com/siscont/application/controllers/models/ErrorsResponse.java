package com.siscont.application.controllers.models;

import java.util.Map;

public class ErrorsResponse {
    private final Map<String, String> errors;
    private final String message;

    public ErrorsResponse() {
        this.errors = null;
        message = null;
    }

    public ErrorsResponse(String message, Map<String, String> errors) {
        this.errors = errors;
        this.message = message;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getMessage() {
        return message;
    }
}

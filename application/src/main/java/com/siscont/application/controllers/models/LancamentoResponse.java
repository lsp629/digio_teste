package com.siscont.application.controllers.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.siscont.application.controllers.models.serializers.LocalDateDeserializer;
import com.siscont.application.controllers.models.serializers.LocalDateSerializer;
import com.siscont.domain.entities.Lancamento;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

public class LancamentoResponse {
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate data;
    private BigDecimal valor;
    private Long contaContabil;

    public LancamentoResponse() {
    }

    public LancamentoResponse(Lancamento lancamento) {
        this.data = lancamento.getData();
        this.valor = lancamento.getValor();
        this.contaContabil = lancamento.getContaContabil().getId();
    }

    public LocalDate getData() {
        return data;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public Long getContaContabil() {
        return contaContabil;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (LancamentoResponse) o;
        return Objects.equals(data, that.data) && Objects.equals(valor
                .setScale(2, RoundingMode.HALF_EVEN), that.valor
                .setScale(2, RoundingMode.HALF_EVEN))
                && Objects.equals(contaContabil, that.contaContabil);
    }

    @Override
    public String toString() {
        return "LancamentoResponse{" +
                ", data=" + data +
                ", valor=" + valor +
                ", contaContabilId=" + contaContabil +
                '}';
    }
}

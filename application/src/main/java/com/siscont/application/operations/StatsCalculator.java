package com.siscont.application.operations;

import com.siscont.application.controllers.models.StatsResponse;
import com.siscont.domain.entities.Lancamento;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StatsCalculator {
    private final List<BigDecimal> valores;
    private final int qtd;

    public StatsCalculator(List<Lancamento> lancamentos) {
        this.valores = mapValores(lancamentos);
        this.qtd = valores.size();
    }

    private List<BigDecimal> mapValores(List<Lancamento> lancamentos) {
        return lancamentos.parallelStream().map(Lancamento::getValor)
                .collect(Collectors.toList());
    }

    private BigDecimal sum() {
        return valores.stream().reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    private BigDecimal calculateAvarage() {
        if (sum().equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }

        return sum().divide(BigDecimal.valueOf(qtd), RoundingMode.HALF_EVEN);
    }

    private BigDecimal getMin() {
        return valores.stream().min(Comparator.naturalOrder()).orElse(BigDecimal.ZERO);
    }

    private BigDecimal max() {
        return valores.stream().max(Comparator.naturalOrder()).orElse(BigDecimal.ZERO);
    }

    private int getQtd() {
        return qtd;
    }

    public StatsResponse calculateStats() {
        return new StatsResponse(sum(), getMin(),
                max(), calculateAvarage(),
                getQtd());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var that = (StatsCalculator) o;
        return qtd == that.qtd && Objects.equals(valores, that.valores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(valores, qtd);
    }
}

package com.siscont.application.controller.api.lancamentos;

import com.siscont.application.controllers.models.*;
import com.siscont.application.operations.StatsCalculator;
import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.entities.Lancamento;
import com.siscont.domain.interfaces.ContaContabilRepository;
import com.siscont.domain.interfaces.LancamentoRepository;
import com.siscont.util.builders.LancamentoMockBuilder;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

public class ApiLancamentoRunStepsTest extends SpringIntegrationTest {
    private static final LocalDate DATA_TESTE = LocalDate.now();
    private static final BigDecimal VALOR_TESTE = BigDecimal.ONE.setScale(2);
    private static final Integer INTEGER_ZERO_TESTE = 0;
    private static final String DESCRICAO_GENERICA = "Descrição genérica";

    private UUID lancamentoId;
    private Long contaContabilId;
    private Set<Lancamento> lancamentos;
    private CreateLancamentoRequest lancamento;
    private WebTestClient.ResponseSpec response;

    @Autowired
    private WebTestClient webClient;

    @Autowired
    private LancamentoRepository lancamentoRepository;

    @Autowired
    private ContaContabilRepository contaContabilRepository;

    @Given("um lançamento válido")
    public void generateLancamento() {
        lancamento = new CreateLancamentoRequest(DATA_TESTE, createContaContabil().getId(), VALOR_TESTE);
    }

    @When("o client faz uma requisição POST {string} com o lançamento válido")
    public void createValidLancamento(String uri) {
        makesPostLancamentoRequest(uri);
    }

    @Given("um lançamento válido com uma conta inválida")
    public void generateLancamentoWithInvalidConta() {
        lancamento = new CreateLancamentoRequest(DATA_TESTE, 9999L, VALOR_TESTE);
    }

    @When("o client faz uma requisição POST {string} com {string}")
    public void tryCreateInvalidLancamento(String uri, String json) {
        response = webClient.post().uri(uri).contentType(MediaType.APPLICATION_JSON)
                .bodyValue(json).exchange();
    }

    @And("uma mensagem dizendo o porque de o lançamento ser inválido")
    public void recievesInvalidLancamentoErrors() {
        var body = response.expectBody(ErrorsResponse.class);
        ErrorsResponse errorsResponse = body.returnResult().getResponseBody();

        assertNotNull(errorsResponse);
        assertNotNull(errorsResponse.getErrors());
        assertFalse(errorsResponse.getErrors().isEmpty());
        assertFalse(errorsResponse.getMessage().isEmpty());
    }

    @When("o client faz uma requisição POST no endpoint {string} com o body vazio")
    public void tryCreateNullLancamento(String uri) {
        response = webClient.post().uri(uri).contentType(MediaType.APPLICATION_JSON)
                .exchange();
    }

    @And("uma mensagem dizendo que o body é inválido")
    public void recievesMessageForNullValue() {
        var body = response.expectBody(ErrorsResponse.class);
        ErrorsResponse errorsResponse = body.returnResult().getResponseBody();

        assertNotNull(errorsResponse);
        assertNull(errorsResponse.getErrors());
        assertFalse(errorsResponse.getMessage().isEmpty());
    }

    @Given("^Um lançamento existente na base de dados")
    public void existingValidLancamento() {
        purgeLancamentos();
        lancamentoId = lancamentoRepository
                .createLancamento(buildLancamento()).getId();
    }

    @Then("o client recebe o id do lançamento criado")
    public void recievesLancamentoId() {
        CreateLancamentoResponse lancamento = response
                .expectBody(CreateLancamentoResponse.class).returnResult().getResponseBody();

        assertNotNull(lancamento);
        assertNotNull(lancamento.getId());
        assertFalse(lancamento.getId().isEmpty());
    }

    @And("o client recebe um status code 201")
    public void recievesCreatedStatusCode() {
        response.expectStatus().isCreated();
    }

    @Then("o client recebe um status code 200")
    public void recievesOkStatusCode() {
        response.expectStatus().isOk();
    }

    @Then("o client recebe um status code 400")
    public void recievesBadRequestStatusCode() {
        response.expectStatus().isBadRequest();
    }

    @When("o client faz uma requisição GET no endpoint {string} com um id válido")
    public void fetchesLancamentoById(String uri) {
        makesGetRequest(format(uri, lancamentoId));
    }

    @Then("o client recebe o lançamento referente ao id")
    public void revievesFetchedLancamento() {
        LancamentoResponse lancamento = response.expectBody(LancamentoResponse.class).returnResult().getResponseBody();

        assertEquals(DATA_TESTE, lancamento.getData());
        assertEquals(VALOR_TESTE, lancamento.getValor());
        assertEquals(contaContabilId, lancamento.getContaContabil());
    }

    @When("o client faz uma requisição GET no endpoint {string} com um id inválido")
    public void fetchsInvalidLancamento(String uri) {
        makesGetRequest(format(uri, UUID.randomUUID().toString()));
    }

    @And("a mensagem {string}")
    public void recievesMessageLancamentoNaoEncontrado(String message) {
        ErrorsResponse errorsResponse = response.expectBody(ErrorsResponse.class).returnResult().getResponseBody();

        assertNotNull(errorsResponse);
        assertNotNull(errorsResponse.getMessage());
        assertEquals(message, errorsResponse.getMessage());
    }

    @Then("o client recebe um status code 404")
    public void recievesNotFoundStatusCode() {
        response.expectStatus().isNotFound();
    }

    @When("o client faz uma requisição GET no endpoint {string} com um id de conta contabil válido")
    public void fetchLancamentosForValidoContaContabil(String uri) {
        makesGetRequest(format(uri, contaContabilId));
    }

    @Then("o client recebe a lista de lançamentos")
    public void recievesLancamentosForContaContabil() {
        recievesLancamentos();
    }

    @When("o client faz uma requisição GET no endpoint {string} com um id de conta contabil inválido")
    public void fetchLancamentosForInvalidContaContabilId(String uri) {
        makesGetRequest(format(uri, new Random().nextLong() * 2));
    }

    @Given("Uma base de dados com lançamentos disponíveis")
    public void insertLancamentosOnDB() {
        purgeLancamentos();
        insertLancamentos(buildLancamentos());
    }

    @When("o client faz uma requisiçao GET no endpoint {string}")
    public void fetchesAllLancamentos(String uri) {
        makesGetRequest(uri);
    }

    private void insertLancamentos(Set<Lancamento> lancamentos) {
        lancamentos.forEach(l -> lancamentoRepository.createLancamento(l));
    }

    @Then("o client recebe uma lista com todos os lançamentos da base de dados")
    public void recievesLancamentos() {
        List<LancamentoResponse> lancamentosExpected = mapLancamentosToLancamentosResponse();
        List<LancamentoResponse> lancamentosCurrent = getLancamentoResponseList();

        assertTrue(lancamentosExpected.containsAll(lancamentosCurrent));
    }

    @Given("Uma base de dados não contendo lançamentos")
    public void purgeLancamentos() {
        lancamentoRepository.purge();
    }

    @Then("o client recebe uma lista vazia")
    public void recievesEmptyList() {
        List<LancamentoResponse> lancamentosCurrent = getLancamentoResponseList();

        assertTrue(lancamentosCurrent.isEmpty());
    }

    @Then("o client recebe as estatísticas dos lançamentos")
    public void recievesLancamentamentoStats() {
        StatsResponse expected = calculateStats();
        StatsResponse current = recievesStats();

        assertEquals(expected, current);
    }

    @Then("o client recebe zero em todos os valores estatísticos")
    public void recievesLancamentosStatsZero() {
        var current = recievesStats();

        assertEquals(BigDecimal.ZERO, current.getMax());
        assertEquals(BigDecimal.ZERO, current.getMin());
        assertEquals(BigDecimal.ZERO, current.getMedia());
        assertEquals(BigDecimal.ZERO, current.getSoma());
        assertEquals(INTEGER_ZERO_TESTE, current.getQtde());
    }

    @When("o client faz uma requisição POST {string} com o lançamento válido e a conta inexistente")
    public void makesRequestWithValidLancamentoAndInvalidConta(String uri) {
        makesPostLancamentoRequest(uri);
    }

    @When("o client faz uma requisição GET no endpoint {string} com um id conta inválido")
    public void fetchesLancamentosInvalidConta(String uri) {
        makesGetRequest(format(uri, 88888L));
    }

    private List<LancamentoResponse> getLancamentoResponseList() {
        ParameterizedTypeReference<LancamentoResponse> typeReference = new ParameterizedTypeReference<>() {};
        return response.expectBodyList(typeReference).returnResult()
                .getResponseBody();
    }

    private StatsResponse recievesStats() {
        return response.expectBody(StatsResponse.class).returnResult().getResponseBody();
    }

    private StatsResponse calculateStats() {
        return new StatsCalculator(new ArrayList<>(lancamentos)).calculateStats();
    }

    public ContaContabil createContaContabil() {
        var contaContabil = saveContaContabil(new ContaContabil(DESCRICAO_GENERICA));
        this.contaContabilId = contaContabil.getId();
        return contaContabil;
    }

    public ContaContabil saveContaContabil(ContaContabil contaContabil) {
        return contaContabilRepository.create(contaContabil);
    }

    public Lancamento buildLancamento() {
        var lancamento = new LancamentoMockBuilder(createContaContabil())
                .withData(DATA_TESTE)
                .withValor(VALOR_TESTE)
                .buildLancamento();
        this.lancamentoId = lancamento.getId();
        lancamentoRepository.createLancamento(lancamento);
        return lancamento;
    }

    public Set<Lancamento> buildLancamentos() {
        var lancamentos = new LancamentoMockBuilder(createContaContabil())
                .buildListaLancamentos(10);
        this.lancamentos = lancamentos;
        return lancamentos;
    }

    private void makesPostLancamentoRequest(String uri) {
        response = webClient.post()
                .uri(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(lancamento), Lancamento.class)
                .exchange();
    }

    private List<LancamentoResponse> mapLancamentosToLancamentosResponse() {
        return lancamentos.stream().map(LancamentoResponse::new)
                .collect(Collectors.toList());
    }

    public void makesGetRequest(String uri) {
        response = webClient.get().uri(uri).exchange();
    }

    @Given("uma conta contabil válida com id 1")
    public void createValidContaContabil() {
        saveContaContabil(new ContaContabil(1L, DESCRICAO_GENERICA));
    }
}

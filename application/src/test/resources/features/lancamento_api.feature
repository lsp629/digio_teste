# new feature
# Tags: optional

Feature: Api de lançamentos
  Um lançamento é um dos recursos mais importantes para a gestão de contas contábeis.
  Desta forma, a existência de uma api para gerenciar os lançamentos permite facilitar não só o registro de lançamentos,
  mas o gerenciamento facilitado dos mesmos dentro de uma conta contábil.

  Scenario Outline: Um client registra um lançamento
    Given uma conta contabil válida com id 1
    When o client faz uma requisição POST "/lancamentos-contabeis" com <json>
    Then o client recebe o id do lançamento criado
    And o client recebe um status code 201

  Examples:
    |json|
    |"{\"data\": 20210110, \"valor\": 10.00, \"contaContabil\": 1 }"|
    |"{\"data\": 20210111, \"valor\": 10.00, \"contaContabil\": 1 }"|
    |"{\"data\": 20210112, \"valor\": 10.00, \"contaContabil\": 1 }"|

  ## 200 ##
  Scenario: Um client busca um lançamento específico
    Given Um lançamento existente na base de dados
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis/%s" com um id válido
    Then o client recebe o lançamento referente ao id
    And o client recebe um status code 200

  Scenario: Um client busca lançamentos de uma conta contábil específica
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis?contaContabil=%s" com um id de conta contabil válido
    Then o client recebe a lista de lançamentos
    And o client recebe um status code 200

  Scenario: Um client busca as estatísticas de todos os lançamentos
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisiçao GET no endpoint "/lancamentos-contabeis/stats"
    Then o client recebe as estatísticas dos lançamentos
    And o client recebe um status code 200

  Scenario: Um client busca as estatísticas de todos os lançamentos
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis/stats/?contaContabil=%s" com um id de conta contabil válido
    Then o client recebe as estatísticas dos lançamentos
    And o client recebe um status code 200

  Scenario: Um client busca as estátiscas mas não existem lançamentos disponíveis
    Given Uma base de dados não contendo lançamentos
    When o client faz uma requisiçao GET no endpoint "/lancamentos-contabeis/stats"
    Then o client recebe zero em todos os valores estatísticos
    And o client recebe um status code 200

  ## 404 ###
  Scenario: Um client tenta buscar as estatísticas dos lançamentos com um id conta inválido
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis/stats?contaContabil=%s" com um id conta inválido
    Then o client recebe um status code 404
    And a mensagem "Conta contábil não existe"

  Scenario: Um client tenta registar um lançamento em uma conta contábil inexistente
    Given um lançamento válido com uma conta inválida
    When o client faz uma requisição POST "/lancamentos-contabeis" com o lançamento válido e a conta inexistente
    Then o client recebe um status code 404
    And a mensagem "Conta contábil não existe"

  Scenario: Um client tenta buscar lançamentos com uma conta contabil inexistente
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis?contaContabil=%s" com um id conta inválido
    Then o client recebe um status code 404
    And a mensagem "Conta contábil não existe"

  Scenario: Um client busca um lançamento com um registro de identificação inválido
    When o client faz uma requisição GET no endpoint "/lancamentos-contabeis/%s" com um id inválido
    Then o client recebe um status code 404
    And a mensagem "Lançamento não encontrado"

  ## 400 ##
  Scenario: Tenta buscar lançamentos sem passar o parâmetro de conta contábil
    Given Uma base de dados com lançamentos disponíveis
    When o client faz uma requisiçao GET no endpoint "/lancamentos-contabeis"
    Then o client recebe um status code 400
    And a mensagem "O parâmetro requerido 'contaContabil' não foi enviado"

  Scenario Outline: Um client tenta registrar um lançamento inválido <json>
    When o client faz uma requisição POST "/lancamentos-contabeis" com <json>
    Then o client recebe um status code 400
    And uma mensagem dizendo o porque de o lançamento ser inválido

    Examples:
      |json|
      |"{\"data\": 20210109, \"valor\": 10.00 }"    |
      |"{\"data\": 20210109, \"contaContabil\": 1 }"|
      |"{\"valor\": 10.00, \"contaContabil\": 1 }"  |

  Scenario: Um client tenta registrar um lançamento nulo
    When o client faz uma requisição POST no endpoint "/lancamentos-contabeis" com o body vazio
    Then o client recebe um status code 400
    And uma mensagem dizendo que o body é inválido
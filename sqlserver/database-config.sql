create database SISTEMACONTABIL;
GO

use SISTEMACONTABIL;
GO

create table conta_contabil (id numeric(19,0) identity not null, descricao varchar(255), primary key (id))
GO
create table lancamento (id binary(255) not null, data datetime not null, hash_code int unique, valor numeric(19,2) not null, conta_contabil_id numeric(19,0) not null, primary key (id))

GO

SET IDENTITY_INSERT conta_contabil ON
GO
INSERT INTO conta_contabil (id, descricao) VALUES (1, 'Alguma descricao')
GO
#run the setup script to create the DB and the schema in the DB
#do this in a loop because the timing for when the SQL instance is ready is indeterminate
while :;
do
  /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $SA_PASSWORD -d master -i database-config.sql
  if [ $? -eq 0 ]
    then
        echo "database-config.sql completed"
        break
    else
        echo "not ready yet..."
        sleep 1
    fi
done
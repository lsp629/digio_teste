package com.siscont.util.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class RandomMoneyGenerator {
    private int range = 10000000;

    public RandomMoneyGenerator() {
    }

    public RandomMoneyGenerator(int range) {
        this.range = range;
    }

    public BigDecimal generate() {
        return BigDecimal.valueOf(Math.abs(new Random().nextDouble() * range)).setScale(2,
                RoundingMode.HALF_EVEN);
    }
}

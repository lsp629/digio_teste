package com.siscont.util.builders;

import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.entities.Lancamento;
import com.siscont.util.operations.RandomMoneyGenerator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LancamentoMockBuilder {
    private LocalDate data;
    private BigDecimal valor;
    private final ContaContabil contaContabil;

    public LancamentoMockBuilder(ContaContabil contaContabil) {
        this.contaContabil = contaContabil;
    }

    public LancamentoMockBuilder withData(LocalDate data) {
        this.data = data;
        return this;
    }

    public LancamentoMockBuilder withValor(BigDecimal valor) {
        this.valor = valor.setScale(2, RoundingMode.HALF_EVEN);
        return this;
    }

    public Set<Lancamento> buildListaLancamentos(int quantidade) {
        var lancamentos = new TreeSet<Lancamento>();
        for (int i = 0; i < quantidade; i++) {
            if (!lancamentos.add(buildLancamento())) {
                i--;
            }
        }

        return lancamentos;
    }

    public Lancamento buildLancamento() {
        return new Lancamento(generateData(), generateValor(), this.contaContabil);
    }

    private BigDecimal generateValor() {
        if (valor == null) {
            return new RandomMoneyGenerator().generate();
        }

        return this.valor;
    }

    private LocalDate generateData() {
        if (data == null) {
            return LocalDate.of(2021, Math.abs(new Random().nextInt(5) + 1),
                    Math.abs(new Random().nextInt(27) + 1));
        }

        return this.data;
    }
}

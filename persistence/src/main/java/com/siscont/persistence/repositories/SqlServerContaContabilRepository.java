package com.siscont.persistence.repositories;

import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.interfaces.ContaContabilRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SqlServerContaContabilRepository extends ContaContabilRepository,
        JpaRepository<ContaContabil, Long> {
    default ContaContabil create(ContaContabil contaContabil) {
        return save(contaContabil);
    }

    default void purge() {
        deleteAll();
    }

    default Optional<ContaContabil> fetchOne(Long id) {
        return findById(id);
    }
}

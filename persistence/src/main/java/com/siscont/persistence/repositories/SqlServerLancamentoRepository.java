package com.siscont.persistence.repositories;

import com.siscont.domain.entities.Lancamento;
import com.siscont.domain.interfaces.LancamentoRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface SqlServerLancamentoRepository extends LancamentoRepository, JpaRepository<Lancamento, UUID>{
    default Lancamento createLancamento(Lancamento lancamento) {
        return save(lancamento);
    }

    default List<Lancamento> fetchAllLancamentos() {
        return findAll();
    }

    default Optional<Lancamento> fetchOneLancamento(String id) {
        return fetchOneLancamento(UUID.fromString(id));
    }

    default Optional<Lancamento> fetchOneLancamento(UUID id) {
        return findById(id);
    }

    default List<Lancamento> fetchLancamentoByContaContabil(Long id) {
        return findByContaContabilId(id);
    }

    default void purge() {
        deleteAll();
    }

    List<Lancamento> findByContaContabilId(Long id);
}

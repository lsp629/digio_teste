package com.siscont.persistence.repositories;

import com.siscont.domain.entities.ContaContabil;
import com.siscont.domain.entities.Lancamento;
import com.siscont.domain.interfaces.ContaContabilRepository;
import com.siscont.domain.interfaces.LancamentoRepository;
import com.siscont.util.builders.LancamentoMockBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class SqlServerLancamentoRepositoryTest {
    @Autowired
    private LancamentoRepository lancamentoRepository;

    @Autowired
    private ContaContabilRepository contaContabilRepository;

    private static final LocalDate DATA = LocalDate.now();
    private static final BigDecimal VALOR = BigDecimal.ONE.setScale(2);
    private static final String DESCRICAO_GENERICA = "Alguma Descrição";

    @BeforeEach
    public void setup() {
        lancamentoRepository.purge();
        contaContabilRepository.purge();
    }

    @Test
    public void createTest() {
        var contaContabil = createContaContabil();

        var lancamento = new LancamentoMockBuilder(contaContabil).withData(DATA)
        .withValor(VALOR).buildLancamento();
        var lancamentoResult = lancamentoRepository.createLancamento(lancamento);
        assertEquals(DATA, lancamentoResult.getData());
        assertEquals(VALOR, lancamentoResult.getValor());
        assertEquals(contaContabil, lancamentoResult.getContaContabil());
    }

    @Test
    public void fetchAllTest() {
        var valor1 = BigDecimal.ONE;
        var valor2 = BigDecimal.TEN;
        var lancamento1 = new Lancamento(DATA, valor1, createContaContabil());
        lancamentoRepository.createLancamento(lancamento1);
        var lancamento2 = new Lancamento(DATA, valor2, createContaContabil());
        lancamentoRepository.createLancamento(lancamento2);
        var lancamento3 = new Lancamento(DATA, VALOR, createContaContabil());

        List<Lancamento> lancamentos = lancamentoRepository.fetchAllLancamentos();
        assertEquals(2, lancamentos.size());
        assertFalse(lancamentos.contains(lancamento3));
        assertTrue(lancamentos.contains(lancamento1));
        assertTrue(lancamentos.contains(lancamento2));
    }

    @Test
    public void fetchOneTest() {
        var contaContabil = createContaContabil();
        var lancamento = new Lancamento(DATA, VALOR, contaContabil);
        var lancamentoId = lancamentoRepository.createLancamento(lancamento).getId();
        var lancamentoResult = lancamentoRepository.fetchOneLancamento(lancamentoId).orElseThrow();
        assertEquals(lancamento, lancamentoResult);
    }

    @Test
    public void fetchByContaContabilTest() {
        var contaContabil = createContaContabil();
        var valor1 = BigDecimal.ONE;
        var valor2 = BigDecimal.TEN;
        var lancamento1 = new Lancamento(DATA, valor1, contaContabil);
        lancamentoRepository.createLancamento(lancamento1);
        var lancamento2 = new Lancamento(DATA, valor2, contaContabil);
        lancamentoRepository.createLancamento(lancamento2);

        List<Lancamento> lancamentos = lancamentoRepository.fetchLancamentoByContaContabil(contaContabil.getId());
        assertTrue(lancamentos.contains(lancamento1));
        assertTrue(lancamentos.contains(lancamento2));
    }

    private ContaContabil createContaContabil() {
        var contaContabil = new ContaContabil(DESCRICAO_GENERICA);
        return contaContabilRepository.create(contaContabil);
    }
}
